output "project_name" {
  value = "${module.build.project_name}"
}

output "project_id" {
  value = "${module.build.project_id}"
}

output "role_arn" {
  value = "${aws_iam_role.default.id}"
}
